import matplotlib.pyplot as plt
import random as rand
import math

#nt = [0]
def W(n,t):
	W = [0]
	s = 0
	for i in range(1, int(n*t+1)):  #need to handle case where nt is not an int
		Xi = rand.randint(-1,1)
		if Xi == 0:
			i-=1
		else:
			s += Xi * 1/math.sqrt(n)
			W.append(s)
			nt.append(i)
	return W

n = [10,200,1000]
t = [0.5,1,2]

for i in range(len(n)):
	nt = [0]
	w = W(n[i],t[i])
	plt.plot(nt, w)
	plt.savefig("W_n={0},t={1}.ps".format(n[i],t[i]))
