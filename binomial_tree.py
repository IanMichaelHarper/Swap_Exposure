import scipy.special

S0 = 12
u = 2
d = 0.5
r = 0.15
K = 21
p = 13.0/30.0
q = 17.0/30.0

sums=0
for i in range(6):
	sums += scipy.special.binom(5, i) * (p**i) * (q**(5-i)) * max(S0*(u**i)*(d**(5-i)) - K, 0)

C = sums/((1+r)**5)	

print "call = " , C

sums=0
for i in range(6):
	sums += scipy.special.binom(5, i) * (p**i) * (q**(5-i)) * max(K - S0*(u**i)*(d**(5-i)), 0)

P = sums/((1+r)**5)	

print "put = ", P

